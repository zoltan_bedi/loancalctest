﻿using System;
using System.Web.Mvc;
using HitelKalk.Models;

namespace HitelKalk.Controllers
{
    public class HomeController : Controller
    {
        public JsonResult CalculateLoan(double purchasePrice, double interestRate, int loanTermYears)
        {
            var calculator = new PaymentCalculator {PurchasePrice = purchasePrice, InterestRate = interestRate, LoanTermYears = loanTermYears};
            return Json(new
            {
                purchasePrice = calculator.PurchasePrice,
                loanTermYears = calculator.LoanTermYears,
                loanSum = Math.Round(calculator.CalculatePayment() * 12 * loanTermYears),
                monthlyPayment = Math.Round(calculator.CalculatePayment()),
                message = string.Format("\"Amennyiben ön {0} Ft összeget vesz fel {1} évre, a havi törlesztő részlet {2} Ft lesz. \"", 
                calculator.PurchasePrice, calculator.LoanTermYears, Math.Round(calculator.CalculatePayment()))
 
            });
        }
        public ActionResult LoanCalculator(double purchasePrice = 0, double interestRate = 10, int loanTermYears = 0)
        {
            if (purchasePrice == 0 || loanTermYears == 0)
            {
                return View();
            }
            var res = CalculateLoan(purchasePrice, interestRate, loanTermYears).Data;
            res = res.ToString().Replace('=', ':');
            return View(res);
        }
    }
}