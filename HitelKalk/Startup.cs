﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HitelKalk.Startup))]
namespace HitelKalk
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
